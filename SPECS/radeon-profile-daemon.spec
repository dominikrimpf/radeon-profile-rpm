Name: radeon-profile-daemon
Url: https://github.com/marazmista/radeon-profile-daemon
License: GPL-2.0
Group: System/Monitoring
AutoReqProv: on
Version: 20190603
Release: 2
Summary: Systemd service for radeon-profile
Source0: https://github.com/marazmista/radeon-profile-daemon/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires: make gcc-c++
BuildRequires: systemd-rpm-macros
BuildRequires: qt5-qtbase-devel

Requires: qt5-qtbase
Requires: radeon-profile
%{?systemd_requires}

%description
System daemon for reading info about GPU clocks and volts and pass this data to radeon-profile so the GUI application can be run as normal user.

%prep
%setup -q

%build
cd radeon-profile-daemon
# https://en.opensuse.org/openSUSE:Build_system_recipes#qmake
QARGS=(radeon-profile-daemon.pro QMAKE_CFLAGS+="%optflags" QMAKE_CXXFLAGS+="%optflags" QMAKE_STRIP="/bin/true")
qmake-qt5 "${QARGS[@]}" || qmake "${QARGS[@]}" || qmake-qt4 "${QARGS[@]}"
make

%install
cd radeon-profile-daemon
install -Dm755 "target/radeon-profile-daemon" "%{buildroot}%{_bindir}/radeon-profile-daemon"
install -Dm644 "extra/radeon-profile-daemon.service" "%{buildroot}%{_unitdir}/radeon-profile-daemon.service"

%post
%systemd_post radeon-profile-daemon.service

%preun
%systemd_preun radeon-profile-daemon.service

%postun
%systemd_pstun_with_restart radeon-profile-daemon.service

%files
%{_bindir}/radeon-profile-daemon
%{_unitdir}/radeon-profile-daemon.service

%changelog
* Sat Jun 06 2020 Dominik Rimpf <dev@d-rimpf.de> - 20190603-2
- Updated SPEC for Fedora 31, 32 and rawhide support

* Fri Jan 29 2016 danysan95@gmail.com
- Port of package to OBS

* Wed May 14 2014 marazmista@gmail.com
- Initial commit

