Name: radeon-profile
Url: https://github.com/marazmista/radeon-profile
License: GPL-2.0
Group: System/Monitoring
AutoReqProv: on
Version: 20200824
Release: 2
Summary: Display info about radeon cards
Source0: https://github.com/marazmista/radeon-profile/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires: make gcc-c++
BuildRequires: qt5-qtbase-devel qt5-linguist qt5-qtcharts-devel
BuildRequires: libXrandr-devel libdrm-devel

Recommends: glx-utils
Suggests: xorg-x11-server-utils

%description
Radeon profile is an user friendly application that allows to read clocks, voltages and temperatures, manage power profiles and fan speed (on HD7000 series cards and above) and overclock your GPU (on Amdgpu driver).
Radeon profile works with the open source radeon driver (xf86-video-ati), the Amdgpu driver (xf86-video-amdgpu or Amdgpu-Pro) and the Catalyst driver (fglrx).


%prep
%setup -q


%build
cd radeon-profile
# https://en.opensuse.org/openSUSE:Build_system_recipes#qmake
QARGS=(radeon-profile.pro QMAKE_CFLAGS+="%optflags" QMAKE_CXXFLAGS+="%optflags" QMAKE_STRIP="/bin/true")
qmake-qt5 "${QARGS[@]}" || qmake "${QARGS[@]}" || qmake-qt4 "${QARGS[@]}"
lrelease-qt5 radeon-profile.pro || lrelease radeon-profile.pro || lrelease-qt4 radeon-profile.pro
make


%install
cd radeon-profile
install -Dm755 "target/radeon-profile" "%{buildroot}%{_bindir}/radeon-profile"
install -Dm644 "extra/radeon-profile.png" "%{buildroot}%{_datadir}/pixmaps/radeon-profile.png"
install -Dm644 "extra/radeon-profile.desktop" "%{buildroot}%{_datadir}/applications/radeon-profile.desktop"
cd translations
for t in $(ls *.qm); do install -Dm644 "$t" "%{buildroot}%{_datadir}/radeon-profile/$t"; done


%if 0%{?suse_version} >= 1140
%post
%desktop_database_post
%endif


%if 0%{?suse_version} >= 1140
%postun
%desktop_database_postun
%endif


%files
%{_bindir}/radeon-profile
%{_datadir}/pixmaps/radeon-profile.png
%{_datadir}/applications/radeon-profile.desktop
%{_datadir}/radeon-profile


%changelog
* Sat Jun 06 2020 Dominik Rimpf <dev@d-rimpf.de> - 20200504-3
- Updated SPEC for Fedora 31, 32 and rawhide support

* Fri Jan 29 2016 danysan95@gmail.com
- Port of OBS package to other Arch / Debian / Ubuntu

* Sun Oct 27 2013 pontostroy@gmail.com
- initial Open Build Service package

* Sat Mar 2 2013 marazmista@gmail.com
-Initial commit
